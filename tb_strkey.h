#ifndef TB_STRKEY_H
#define TB_STRKEY_H

#include <termbox.h>

/* from termbox.h, better access indirectly using strkey() */
static const char *const key_codes[] = {
	/* 0x00 -- 0x7f */
	[TB_KEY_CTRL_2] = "^2 / ~",
	[TB_KEY_CTRL_A] = "^A",
	[TB_KEY_CTRL_B] = "^B",
	[TB_KEY_CTRL_C] = "^C",
	[TB_KEY_CTRL_D] = "^D",
	[TB_KEY_CTRL_E] = "^E",
	[TB_KEY_CTRL_F] = "^F",
	[TB_KEY_CTRL_G] = "^G",
	[TB_KEY_CTRL_H] = "^H / ^<BS>",
	[TB_KEY_CTRL_I] = "^I / ^<TAB>",
	[TB_KEY_CTRL_J] = "^J",
	[TB_KEY_CTRL_K] = "^K",
	[TB_KEY_CTRL_L] = "^L",
	[TB_KEY_CTRL_M] = "^M / ^<ENTER>",
	[TB_KEY_CTRL_N] = "^N",
	[TB_KEY_CTRL_O] = "^O",
	[TB_KEY_CTRL_P] = "^P",
	[TB_KEY_CTRL_Q] = "^Q",
	[TB_KEY_CTRL_R] = "^R",
	[TB_KEY_CTRL_S] = "^S",
	[TB_KEY_CTRL_T] = "^T",
	[TB_KEY_CTRL_U] = "^U",
	[TB_KEY_CTRL_V] = "^V",
	[TB_KEY_CTRL_W] = "^W",
	[TB_KEY_CTRL_X] = "^X",
	[TB_KEY_CTRL_Y] = "^Y",
	[TB_KEY_CTRL_Z] = "^Z",
	[TB_KEY_ESC   ] = "<ESC> / ^[ / ^3",
	[TB_KEY_CTRL_4] = "^4",
	[TB_KEY_CTRL_5] = "^5 / ^]",
	[TB_KEY_CTRL_6] = "^6",
	[TB_KEY_CTRL_7] = "^7 / ^/ / ^_",
	[TB_KEY_SPACE ] = "<SPACE>",
	[TB_KEY_CTRL_8] = "^8 / ^<BSP2>",
	/* 0xFFFF-27--0xFFFF-0, we map them to 0x1FF-27--0x1FF to save space */
	[TB_KEY_F1               & 0x1FF ] = "<F1>",
	[TB_KEY_F2               & 0x1FF ] = "<F2>",
	[TB_KEY_F3               & 0x1FF ] = "<F3>",
	[TB_KEY_F4               & 0x1FF ] = "<F4>",
	[TB_KEY_F5               & 0x1FF ] = "<F5>",
	[TB_KEY_F6               & 0x1FF ] = "<F6>",
	[TB_KEY_F7               & 0x1FF ] = "<F7>",
	[TB_KEY_F8               & 0x1FF ] = "<F8>",
	[TB_KEY_F9               & 0x1FF ] = "<F9>",
	[TB_KEY_F10              & 0x1FF ] = "<F10>",
	[TB_KEY_F11              & 0x1FF ] = "<F11>",
	[TB_KEY_F12              & 0x1FF ] = "<F12>",
	[TB_KEY_INSERT           & 0x1FF ] = "<INS>",
	[TB_KEY_DELETE           & 0x1FF ] = "<DEL>",
	[TB_KEY_HOME             & 0x1FF ] = "<HOME>",
	[TB_KEY_END              & 0x1FF ] = "<END>",
	[TB_KEY_PGUP             & 0x1FF ] = "<PGUP>",
	[TB_KEY_PGDN             & 0x1FF ] = "<PGDN>",
	[TB_KEY_ARROW_UP         & 0x1FF ] = "<ARROW_UP>",
	[TB_KEY_ARROW_DOWN       & 0x1FF ] = "<ARROW_DOWN>",
	[TB_KEY_ARROW_LEFT       & 0x1FF ] = "<ARROW_LEFT>",
	[TB_KEY_ARROW_RIGHT      & 0x1FF ] = "<ARROW_RIGHT>",
	[TB_KEY_MOUSE_LEFT       & 0x1FF ] = "<MOUSE_LEFT>",
	[TB_KEY_MOUSE_RIGHT      & 0x1FF ] = "<MOUSE_RIGHT>",
	[TB_KEY_MOUSE_MIDDLE     & 0x1FF ] = "<MOUSE_MIDDLE>",
	[TB_KEY_MOUSE_RELEASE    & 0x1FF ] = "<MOUSE_RELEASE>",
	[TB_KEY_MOUSE_WHEEL_UP   & 0x1FF ] = "<MOUSE_WHEEL_UP>",
	[TB_KEY_MOUSE_WHEEL_DOWN & 0x1FF ] = "<MOUSE_WHEEL_DOWN>",
};

/* Translate keycode and/or unicode codepoint to string representation.
 * Returned pointer is invalid on next access to strkey().
 */
static inline const char *strkey(int key, uint32_t ch)
{
	if (key) {
		key &= 0x1FF;
		return (key_codes[key]);
	} else {
		/* UTF-8 keys are maximum 4 bytes */
		static char utf8char[5];
		int len = tb_utf8_unicode_to_char(utf8char, ch);
		utf8char[len] = '\0';
		return (utf8char);
	}
}

#endif
