#include "tb_strkey.h"
#include <termbox.h>
#include <sys/types.h>
#include <unistd.h>
#include <assert.h>
#include <errno.h>
#include <inttypes.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

/* Configurable items */
#define CONFIG_PROGNAME tb_test
#define CONFIG_POLL_RATE_MS (5)
/* ----------------- */


/* utility macros */
#define xstr(s) str(s)
#define str(s) #s
#define max(a, b) ((a) > (b) ? (a) : (b))
#define min(a, b) ((a) <= (b) ? (a) : (b))
#define COLOR_BLACK 0
#define COLOR_WHITE 215
#define COLOR_RED 144
#define COLOR_ORANGE 186
#define COLOR_YELLOW 198
#define COLOR_GREEN 54
#define COLOR_BLUE 10
#define COLOR_PURPLE 75
#define COLOR_PINK 182
#define COLOR_BROWN 36

/* Global constants, typed for non-strings, macros for some strings to use compile-time concat */
#define PROGNAME xstr(CONFIG_PROGNAME)
static const char LOGFILE[] = "/tmp/"PROGNAME".log";
static const char PICFILE[] = "pic.ppm";
static const int POLL_RATE_MS = CONFIG_POLL_RATE_MS;

/* Global variables, shouldn't be many */
static FILE *log;
static FILE *pic;
static uint16_t FG;
static uint16_t BG;
static int CANVAS_WIDTH;
static int CANVAS_HEIGHT;
static struct canvas_cell *CANVAS;

//struct canvas {
//	uint16_t *_;
//	size_t height;
//	size_t width;
//};

struct canvas_cell {
	uint16_t bg;
};

void copy_canvas(void){
	int w = min(CANVAS_WIDTH,tb_width());
	int h = min(CANVAS_HEIGHT,tb_height());
	struct tb_cell *output = tb_cell_buffer();

	for (int a=0; a<h; a++) {
		for (int b=0; b<w; b++) {
			struct canvas_cell *ccell = &CANVAS[a*CANVAS_WIDTH+b];
			output[a*tb_width()+b].bg = ccell->bg;
		}
	}

}
	
void tb_draw(int x, int y){
	CANVAS[y*CANVAS_WIDTH+x].bg = FG;
	copy_canvas();
	
}

void tb_fill(uint16_t color) {
	
	fprintf(log, "tb_fill: initialized function\n");

	for (int y=0; y < CANVAS_HEIGHT; y++) {
		for (int x=0; x < CANVAS_WIDTH; x++) {
			CANVAS[y*CANVAS_WIDTH+x].bg = color;
		}
	}

	copy_canvas();

}

static void fill_space() {
	fprintf(log, "fill_space: initiated function\n");

	for ( int y = min(CANVAS_HEIGHT, tb_height()); y < max(CANVAS_HEIGHT, tb_height()); y++ ) {
		for ( int x = 0; x < max(CANVAS_WIDTH, tb_width()); x++) {
			struct tb_cell *cell;
			cell->bg = COLOR_RED;
			tb_put_cell(x, y, cell);
		}
	}
}

static void color_pick(int x, int y) {
	FG = CANVAS[y*CANVAS_WIDTH+x].bg; 
}

static void save_file() {
	fprintf(pic, "P3\n %d %d 255\n", CANVAS_WIDTH, CANVAS_HEIGHT);
	int r,g,b;

	for (int i=0; i<CANVAS_WIDTH*CANVAS_HEIGHT; i++) {

		switch (CANVAS[i].bg) {
		case COLOR_WHITE:
			r=255 ; g=255 ; b=255 ;
			break;
		case COLOR_BLACK:
			r=0;g=0;b=0;
			break;
		case COLOR_RED:
			r=215;g=0;b=0;
			break;
		case COLOR_ORANGE:
			r=255;g=95;b=0;
			break;
		case COLOR_YELLOW:
			r=255;g=175;b=0;
			break;
		case COLOR_GREEN:
			r=95;g=175;b=0;
			break;
		case COLOR_BLUE:
			r=0;g=95;b=215;
			break;
		case COLOR_PURPLE:
			r=135;g=0;b=175;
			break;
		case COLOR_PINK:
			r=255;g=0;b=135;
			break;
		case COLOR_BROWN:
			r=95;g=0;b=0;
			break;
		default:
			r=0;g=0;b=0;
			break;
		}

		fprintf(pic, "%d %d %d \n", r, g, b);
	}
}

/**
 * Handles events
 * returns -1 to signal program exit
 */
int event_handler(const struct tb_event *const ev)
{
	switch (ev->type) {
	case TB_EVENT_KEY:
		fprintf(log, "tb_peek_event: Key pressed (%s)\n", strkey(ev->key, ev->ch));
		switch (ev->ch) {
		case 'q':
			return (-1);
			break;
		case 'c':
			tb_fill(BG);
			break;
		case 's':
			save_file();
			break;
		case '0':
			FG = COLOR_WHITE;
			break;
		case '1':
			FG = COLOR_BLACK;
			break;
		case '2':
			FG = COLOR_RED;
			break;
		case '3':
			FG = COLOR_ORANGE;
			break;
		case '4':
			FG = COLOR_YELLOW;
			break;
		case '5':
			FG = COLOR_GREEN;
			break;
		case '6':
			FG = COLOR_BLUE;
			break;
		case '7':
			FG = COLOR_PURPLE;
			break;
		case '8':
			FG = COLOR_PINK;
			break;
		case '9':
			FG = COLOR_BROWN;
			break;
		default:
			break;
		}
		break;
	case TB_EVENT_RESIZE:
		fill_space();
		break;
	case TB_EVENT_MOUSE:
		if (ev->key == TB_KEY_MOUSE_LEFT) {
			if (ev->x < CANVAS_WIDTH && ev->y < CANVAS_HEIGHT) {
				tb_draw(ev->x, ev->y);
			}
		} else if (ev->key == TB_KEY_MOUSE_RIGHT) {
			uint16_t TEMP = FG;
			FG = BG;
			BG = TEMP;
		} else if (ev->key == TB_KEY_MOUSE_WHEEL_UP) {
			if (FG < 215) { FG++; }
		} else if (ev->key == TB_KEY_MOUSE_WHEEL_DOWN) {
			if ( FG > 0) { FG--; }
		} else if (ev->key == TB_KEY_MOUSE_MIDDLE) {
			color_pick(ev->x,ev->y);
		}
		
		break;
	default:
		assert(false);
	}
	return (0);
}

int main(void)
{
	int exit_code = 0;
	log = fopen(LOGFILE, "w");
	if (!log) {
		perror(LOGFILE);
		exit_code = 1;
		goto exit_main;
	}
	/* log should be unbuffered */
	setbuf(log, NULL);

	pid_t pid = getpid();
	fprintf(log, PROGNAME": pid = %"PRIdMAX"\n", (intmax_t) pid);

	const int err = tb_init();
	if (err < 0) {
		switch (err) {
		case TB_EUNSUPPORTED_TERMINAL:
			fprintf(stderr, "tb_init: Unsupported Terminal\n");
			exit_code = 1;
			goto close_log;
		case TB_EFAILED_TO_OPEN_TTY:
			fprintf(stderr, "tb_init: Failed to open tty\n");
			exit_code = 1;
			goto close_log;
		case TB_EPIPE_TRAP_ERROR:
			fprintf(stderr, "tb_init: Couldn't open pipe\n");
			exit_code = 1;
			goto close_log;
		default:
			assert(false);
		}
	}

	CANVAS_WIDTH = tb_width();
	CANVAS_HEIGHT = tb_height() - 1;
	fprintf(log, PROGNAME": dimensions (WxH): %d x %d\n", CANVAS_WIDTH, CANVAS_HEIGHT);

	/* keyboard input only */
	tb_select_input_mode(TB_INPUT_MOUSE);
	/* default 8 colors mode */
	tb_select_output_mode(TB_OUTPUT_216);

	FG = COLOR_BLACK;
	BG = COLOR_WHITE;
	CANVAS = malloc((long unsigned int)(CANVAS_WIDTH*CANVAS_HEIGHT) * sizeof (*CANVAS));
	tb_fill(COLOR_WHITE);

	pic = fopen(PICFILE, "w");

	while (true) {
		/* backbuffer <-> frontbuffer */
		tb_present();

		struct tb_event ev;
		const int evtype = tb_peek_event(&ev, POLL_RATE_MS);
		if (evtype == -1) {
			fprintf(log, "tb_peek_event: Unknown error\n");
			exit_code = 1;
			goto close_tb;
		}
		if (evtype == 0) { continue; }

		if (event_handler(&ev) == -1) {
			fprintf(log, PROGNAME": Ending program\n");
			goto close_tb;
		}
	}

close_tb:
	tb_shutdown();
close_log:
	fclose(log);
exit_main:
	return (exit_code);
}
